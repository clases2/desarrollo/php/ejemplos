<?php

// Funciones de mi aplicacion.
function render($vista, $parametros) {
    extract($parametros);
    include "./views/$vista.php";
}

// Cargo el controlador por defecto.
require "controllers/siteController.php";

// Gestiono la accion del controlador a ejecutar.
$urlCompleta = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
//Cuando el formulario envia datos los elimino de la URL.
$separoParametros = explode("?", $urlCompleta);
//Me quedo solo con la URL sin los parametros.
$url = $separoParametros[0]; //Aqui tengo la URL.
// Mi URL seria algo asi: http://localhost/ejemplos/index.php/accion
// Me quedo con el texto que esta detras de la ultima "/" de la URL (accion).
$accion = substr($url, strrpos($url, "/") + 1); //strrpos busca un caracter desde el final del string.
//Establezco la accio  por defecto. La accion la primera vez que carga la web.
if ($accion == "index.php") {
    $accion = "index";
}
//En esta aplicacion todas las aplicaciones tienen que tener el prefijo action.
$accion = "action" . ucfirst($accion);

