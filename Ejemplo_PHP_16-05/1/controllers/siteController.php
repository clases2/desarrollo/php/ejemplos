<?php

function actionSalida() {
    //Renderizo la vista mostrar y le paso como parametro el nombre.
    render("mostrar", [
        "nombre" => $_GET["nombre"]
    ]);
}

function actionEntrada() {
    //Renderizo la vista formulario y paso como parqametro el nombre vacio.
    render("formulario", [
        "nombre" => ""
    ]);
}

function actionFormulario() {
    if (isset($_GET["boton"])) {
        //Si he pulsado el boton del formulario, llamo a la accion de salida.
        actionSalida();
    } else {
        //Si no he pulsao el boton, llamo la accion entrara.
        actionEntrada();
    }
}

function actionIndex() {
    render("index", []);
}
