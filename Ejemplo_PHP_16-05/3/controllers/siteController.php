<?php

function actionIndex() {
    //Ejemplo de aplicacion con datos MVC.
    //Inicio.
    render("index", []);
}

function actionListar() {
    //Tabla con los datos sin foto.
    //Mostrar.
    render("mostrar", []);
}

function actionTodo() {
    //Tabla con los datos con foto.
    //Todo.
    render("todo", []);
}
