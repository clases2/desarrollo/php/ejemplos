<?php
require "vendor/aplicacion.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <ul>
            <li>
                <a href="./index">Index</a>
            </li>
            <li>
                <a href="./quienes">Quienes somos</a>
            </li>
            <li>
                <a href="./donde">Donde estamos</a>
            </li>
            <li>
                <a href="./conocenos">Conocenos</a>
            </li>
            <li>
                <a href="./contacto">Contactanos</a>
            </li>
        </ul>
        <?php
        //Tengo una variable accion que tiene el nombre de la accion a ejecutar. accion="actionIndex"
        $accion();
        ?>
    </body>
</html>
