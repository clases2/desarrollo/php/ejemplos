<?php

function actionIndex() {
    render("index", []);
}

function actionQuienes() {
    $componentes = ["alumno1", "alumno2", "alumno3", "Jose", "Ana"];
    render("equipo", ["equipo" => $componentes]);
}

function actionDonde() {
    render("mapa", []);
}

function actionConocenos() {
    render("conocer", [
        "titulo" => "Empresa de Formacion Actual",
        "descripcion" => "Descripcion de la empresa.",
        "foto" => "../imgs/Empresa.jpg"]);
}

function actionContacto() {
    //Es la primera vez que cargo la vista.
    if (!isset($_GET["boton"])) {
        render("formulario", []);
    } else {
        //Cuando he pulsado el boton.
        render("mensaje", []);
    }
}
