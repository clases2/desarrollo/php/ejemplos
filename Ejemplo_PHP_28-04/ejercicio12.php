<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo SVG</title>
    </head>
    <body>
        <?php
        $circulos = [
            10, 20, 30
        ];

        $cuadrados = [
            20, 40
        ];

        foreach ($circulos as $radio) {
            //Principio impresion de circulos
            ?> 
            <?xml version = "1.0" encoding = "UTF-8" standalone = "no"?>

            <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                 width="140" height="140" viewBox="-10 -10 140 140"
                 style="background-color: white">
            <circle cx="60" cy="60" r="<?= $radio ?>" fill="black" />
            </svg> 


            <?php
        }//Final de impresion de circulos
        ?>

        <?php
        foreach ($cuadrados as $cuadro) {
            //Comienzo de impresion de cuadrados
            ?>
            <?xml version="1.0" encoding="UTF-8" standalone="no"?>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                 width="140" height="140" viewBox="-10 -10 140 140"
                 style="background-color: white">
            <rect x="10" y="10" width="<?= $cuadro ?>" height="<?= $cuadro ?>"/>
            </svg>

            <?php
        }//Final de impresion de cuarados
        ?>

    </body>
</html>
