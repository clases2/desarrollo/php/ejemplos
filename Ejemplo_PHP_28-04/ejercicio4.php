<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <form method="get">
            <div>
                <label for="nombre">Nombre</label>
                <input type="text" id="nombre" name="nombre" class="form-control">
            </div>
            <div>
                <label for="edad">Edad</label>
                <input type="number" id="edad" name="edad" class="form-control">
            </div>
            <div>
                <label for="poblacion">Poblacion</label>
                <input type="text" id="poblacion" name="poblacion" class="form-control">
            </div>
            <button class="btn btn-primary">Enviar</button>
        </form>

        <?php
        if ($_GET) {
            //principio comprobacion de si llegan datos
            
            $nombre = $_GET["nombre"];
            $edad = $_GET["edad"];
            $poblacion = $_GET["poblacion"];
            ?>
            <table>
                <tr>
                    <td>Nombre</td>
                    <td><?= $nombre ?></td>
                </tr>
                <tr>
                    <td>Edad</td>
                    <td><?= $edad ?></td>
                </tr>
                <tr>
                    <td>Poblacion</td>
                    <td> <?= $poblacion ?> </td>
                </tr>
            </table>
            <?php
            // final de comprobacion de si llegan datos
        }
        ?>
    </body>
</html>
