<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // array enumerado
        $menu=[
            "Home",
            "Quienes somos",
            "Productos"
        ];
        
        // crear un menu con el array enumerado
        echo "<ul>";
        foreach ($menu as $valor){
            echo "<li>$valor</li>";
        }
        echo "</ul>";
        
        ?>
        
        <ul>
        <?php
            foreach ($menu as $valor){
        ?>
           <li><?= $valor ?> </li>
        <?php
        }
        ?>
        </ul>
        
        
        
        <?php
        // array asociativo
        $menu1=[
            "Home" => "index.php",
            "Quienes somos" => "quienes.php",
            "Productos" => "productos.php"
        ];

        // crear un menu desde un array asociativo
        echo "<ul>";
        foreach ($menu1 as $indice=>$valor){
            echo "<li><a href=\"$valor\">$indice</a></li>";
        }
        echo "</ul>";
        ?>
        
        <ul>
        <?php
        foreach ($menu1 as $indice=>$valor){
        ?>
            <li><a href="<?= $valor?>"><?= $indice ?></a></li>
        <?php
        }
        ?>
        </ul>
        
        
        <?php
        // array de dos dimensiones
        // primera dimension es array enumerado
        // segunda dimension es array asociativo
        $datos=[
            [
                "id" => 1,
                "nombre" => "Jose"
            ],
            [
                "id" => 2,
                "nombre" => "Ana"
            ],
        ];
               
        
        //imprimir una tabla con los datos
        
        echo "<table>";
        
        // imprimir cabeceras
        echo "<tr>";
        foreach ($datos[0] as $titulo=>$valor){
            echo "<td>$titulo</td>";
        }
        echo "</tr>";
        //fin de imprimir cabeceras
        
        // inicio impresion datos
        foreach ($datos as $registro){
            // inicio de impresion de registro
            echo "<tr>";
            foreach ($registro as $valor){
               echo "<td>$valor</td>"; 
            }
            echo "</tr>";
            // final de impresion de registro
            
        }
        
        // final impresion datos
        
        echo "</table>";
        
        
        ?>
        
                
        
        <table>
            <tr>
        <?php
        // imprimir cabeceras
        foreach ($datos[0] as $titulo=>$valor){
        ?>
            <td><?= $titulo ?></td>
        <?php
        }
        //fin de imprimir cabeceras
        ?>
            </tr>
        
        <?php
        // inicio impresion datos
        foreach ($datos as $registro){
            // inicio de impresion de registro
        ?>
            <tr>
        <?php
            foreach ($registro as $valor){
        ?>
                <td><?= $valor ?></td>
        <?php
            }
        ?>
            </tr>
            
        <?php
        // final de impresion de registro
        }
        
        // final impresion datos
        ?>
        
        </table>
        
    </body>
</html>
