<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Funciones</title>
    </head>
    <body>
        <?php

        //funcion sin parametros
        function dibujar() {
            echo "<div style=\"background-color:#CCC;width:100px;height:100px;margin: 10px;\"></div>";
        }

        function dibujar1() {
            ?>
            <div style="background-color:#CCC; width:100px; height:100px;margin: 10px;"></div>
            <?php
        }

        function dibujar2() {
            $caja = "<div style=\"background-color:#CCC;width:100px;height:100px;margin: 10px;\"></div>";
            echo $caja;
        }

        function dibujar3() {
            $caja = file_get_contents("Templates/Caja.html");
            echo "$caja";
        }

        function dibujar4() {
            $caja = include "Templates/Caja.html";
            var_dump($caja);
        }

        dibujar();
        dibujar1();
        dibujar2();
        dibujar3();
        dibujar4();
        ?>
    </body>
</html>
