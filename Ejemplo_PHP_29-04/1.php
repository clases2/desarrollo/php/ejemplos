<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        /**
         * Funcion que recibe una array de numeros y devuelve el numero mayor.
         * @param array $numeros Numeros a evaluar.
         * @return int Numero mayor.
         */
        function uno($numeros = []) {

            $numero = max($numeros);
            return $numero;
        }

        /**
         * Funcion que recibe una array de numeros y devuelve el numero mayor.
         * @param array $numeros Numeros a evaluar.
         * @return int Numero mayor.
         */
        function unoMia($numeros = []) {
            $numero = $numeros[0];
            foreach ($numeros as $valor) {
                if ($valor > $numero) {
                    $numero = $valor;
                }
            }
            return $numero;
        }

        /**
         * Funcion que recibe un array de numeros y los ordena de forma descendente.
         * @param array $numeros Numeros a ordenar.
         * @return string Numeros ordenados.
         */
        function ordenar($numeros = []) {
            rsort($numeros);
            $ordenando = $numeros;
            $ordenado = implode(", ", $ordenando);

            return $ordenado;
        }

        //Ordenar el array de forma ascendente.
        function ordenarDos($numeros) {
            $ordenando = 0;

            do { //Empieza el bucle.
                $ordenado = true;
                for ($i = 0; $i < (count($numeros) - 1); $i++) {
                    if ($numeros[$i] > $numeros[$i + 1]) {
                        $ordenando = $numeros[$i]; //Guardo el valor mas grande que se va a mover.
                        $numeros[$i] = $numeros[$i + 1]; //Asigno la nueva posicion del valor menor.
                        $numeros[$i + 1] = $ordenando; //Devuelvo el valor mas grande a la posicion donde estaba el mas pequeño.
                        $ordenado = false;
                    }
                }//Termina el for.
            } while ($ordenado == false); //Termina el bucle.

            $resultado = implode(", ", $numeros);
            return $resultado;
        }

        $array = [1, 32, 643, 12, 63, 70, 0, 124, 800000000];
        $array2 = [23, 34, 4, 67, 3, 2];
        echo uno($array) . "<br>";
        echo unoMia($array) . "<br>";
        echo ordenar($array) . "<br>";
        echo ordenarDos($array2) . "<br>";
        ?>
    </body>
</html>