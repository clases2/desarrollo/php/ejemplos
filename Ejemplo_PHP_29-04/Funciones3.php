<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Reemplazando caracteres</title>
    </head>
    <body>
        <?php
        $a = "Ejemplo de clase";
        //Sustituor la O por un -
        $final = str_replace(
                "o", //Caracter a sustituir
                "-", //Caracter reemplazado
                $a); //El texto done sustituye los caracteres

        echo $final; //La salida es "Ejempl- de clase"
        //Sustituir todas las E por -
        $final = str_replace("e", "-", $a);
        echo $final;

        //Sustituye todas las e por - tanto mayusculas como minusculas
        $final = str_ireplace("e", "-", $a);

        //Sustituir clase por aula
        $final = str_replace("clase", "aula", $a);
        echo $final;
        ?>
    </body>
</html>
