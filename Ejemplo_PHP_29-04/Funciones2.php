<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Funciones con argumentos</title>
    </head>
    <body>
        <?php

        //funcion con argumentos
        function dibujar($texto) {
            echo "<div style=\"background-color:#CCC;width:100px;height:100px;margin: 10px;\">$texto</div>";
        }

        function dibujar1($texto) {
            ?>
            <div style="background-color:#CCC; width:100px; height:100px;margin: 10px;">
                <?= $texto ?>
            </div>
            <?php
        }

        function dibujar2($texto) {
            $caja = "<div style=\"background-color:#CCC;width:100px;height:100px;margin: 10px;\">$texto</div>";

            echo $caja;
        }

        function dibujar3($texto) {
            $caja = file_get_contents("Templates/Caja.plantilla");
            //Sustituir {{texto}} por $texto
            $final = str_replace("{{texto}}", $texto, $caja);
            var_dump($caja);
            var_dump($final);
            echo $caja;
        }

        function dibujar4($texto) {
            include "Templates/Caja.inc";
        }

        //Sustituir la o por un -
        //Sustituir todas las e por -
        //Sustituir clase por aula
        $a = "Ejemplo de clase";

        dibujar("hola clase");
        dibujar1("Hola clase1");
        dibujar2("Hola clase2");
        dibujar3("Hola clase3");
        dibujar4("Hola clase4");
        ?>
    </body>
</html>
