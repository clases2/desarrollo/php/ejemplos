<?php
    require './Formulario3_1.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        
        <?php
        
        // inicializo las variables
        $numero1=0;
        $numero2=0;
        $resultado = [
                "cociente" => 0,
                "resto" => 0
        ];
        
        // compruebo si he pulsado el boton
        if (isset($_POST["calcular"])) {
           $numero1 = $_POST["numero1"];
           $numero2 = $_POST["numero2"];
           calcular($numero1, $numero2, $resultado);
        }
        
        // mostrar la vista    
        render($numero1, $numero2, $resultado);
        
        ?>

    </body>
</html>
