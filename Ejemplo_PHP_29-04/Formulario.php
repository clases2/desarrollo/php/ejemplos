<?php

/**
 * Funcion para generar un formulario desde una plantilla.
 * @param string $titulo Titulo del formulario.
 * @param string $etiqueta Texto junto al input.
 * @param string $nombre Id del input.
 * @return string Formulario con el control generado.
 */
function dibujar($titulo, $etiqueta, $nombre) {
    // Utilizamos Formulario.inc como una plantilla con file_get_content
    $formulario = file_get_contents("Templates/Formulario.inc");
    $formulario = str_ireplace("{{titulo}}", $titulo, $formulario);
    $formulario = str_ireplace("{{etiqueta}}", $etiqueta, $formulario);
    $formulario = str_ireplace("{{nombre}}", $nombre, $formulario);
    return $formulario;
}

/**
 * Funcion para generar un formulario desde una plantilla.
 * @param string $titulo Titulo del formulario.
 * @param string $etiqueta Texto junto al input.
 * @param string $nombre Id del input.
 * @return string Formulario con el control generado.
 */
function dibujar1($titulo, $etiqueta, $nombre) {
    include './Templates/Formulario1.inc';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo dibujar("Introduccion de Datos", "Numero 1", "numero1");
        echo dibujar("Introduccion de Numero", "Numero 2", "numero2");
        echo dibujar1("Introduccion de Numero 2", "Numero 3", "numero3");
        ?>
    </body>
</html>
