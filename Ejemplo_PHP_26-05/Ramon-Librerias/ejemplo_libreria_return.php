<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>

    </head>
    <body>
        Mediante este ejemplo tenemos un metodo para que las librerias en vez de mostrar el contenido lo cargen en una variable
        <?php

        function retornaContenido() {
            ob_start();
            require './inc/codigo.inc';
            return ob_get_clean();
        }

        $contenido = retornaContenido();
        ?>

        <?= $contenido ?>
        <?= $contenido ?>
    </body>
</html>
