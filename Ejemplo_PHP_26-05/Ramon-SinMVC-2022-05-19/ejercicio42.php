<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <title>Galeria 2</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><i class="fa fa-home" aria-hidden="true"></i>Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ejercicio1.php">Ejercicio 1</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ejercicio2.php">Ejercicio 2</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ejercicio 3
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="ejercicio3s.php">Suma</a>
                            <a class="dropdown-item" href="ejercicio3p.php">Producto</a>
                            <a class="dropdown-item" href="ejercicio3.php">Ambos</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ejercicio 4
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="ejercicio41.php">Fotos 1</a>
                            <a class="dropdown-item" href="ejercicio42.php">Fotos 2</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container-fluid my-4">
            <div class="row">
                <div class="col-lg-3">
                    <div class="card mb-3">
                        <img class="card-img-top" src="./imgs/f5.jpg" data-toggle="modal" data-target="#foto1">
                        <div class="card-body">
                            <h5 class="card-title">Foto 5</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, repudiandae, fuga magni inventore blanditiis suscipit animi odio ex ratione rem voluptatibus odit harum sit libero natus amet laboriosam saepe nemo!</p>
                            <p class="card-text"><small class="text-muted">12 Enero 2022</small></p>
                        </div>
                    </div>
                </div>
                <div id="foto1" class="modal fade">
                    <div class="modal-dialog">
                        <div class="model-content">
                            <img src="./imgs/f5.jpg" class="modal-lg" data-dismiss="modal">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card mb-3">
                        <img class="card-img-top" src="./imgs/f6.jpg" data-toggle="modal" data-target="#foto2">
                        <div class="card-body">
                            <h5 class="card-title">Foto 6</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, repudiandae, fuga magni inventore blanditiis suscipit animi odio ex ratione rem voluptatibus odit harum sit libero natus amet laboriosam saepe nemo!</p>
                            <p class="card-text"><small class="text-muted">12 Enero 2022</small></p>
                        </div>
                    </div>
                </div>
                <div id="foto2" class="modal fade">
                    <div class="modal-dialog modal-lg">
                        <div class="model-content">
                            <img src="./imgs/f6.jpg" class="mw-100" data-dismiss="modal">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card mb-3">
                        <img class="card-img-top" src="./imgs/f7.jpg">
                        <div class="card-body">
                            <h5 class="card-title">Foto 7</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, repudiandae, fuga magni inventore blanditiis suscipit animi odio ex ratione rem voluptatibus odit harum sit libero natus amet laboriosam saepe nemo!</p>
                            <p class="card-text"><small class="text-muted">12 Enero 2022</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card mb-3">
                        <img class="card-img-top" src="./imgs/f8.jpg">
                        <div class="card-body">
                            <h5 class="card-title">Foto 8</h5>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, repudiandae, fuga magni inventore blanditiis suscipit animi odio ex ratione rem voluptatibus odit harum sit libero natus amet laboriosam saepe nemo!</p>
                            <p class="card-text"><small class="text-muted">12 Enero 2022</small></p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Optional JavaScript -->
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>