<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <title>Ejercicio 2</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><i class="fa fa-home" aria-hidden="true"></i>Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ejercicio1.php">Ejercicio 1</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ejercicio2.php">Ejercicio 2</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ejercicio 3
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="ejercicio3s.php">Suma</a>
                            <a class="dropdown-item" href="ejercicio3p.php">Producto</a>
                            <a class="dropdown-item" href="ejercicio3.php">Ambos</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ejercicio 4
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="ejercicio41.php">Fotos 1</a>
                            <a class="dropdown-item" href="ejercicio42.php">Fotos 2</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="my-4 container-fluid">
            <?php
            if (isset($_GET["ejercicio2"])) {
                ?>
                <div class="col-lg-8 row mx-auto">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="card-header">Email</div>
                            <div class="card-body">
                                <p class="card-text"><?= $_GET["correo"] ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="card-header">Password</div>
                            <div class="card-body">
                                <p class="card-text"><?= $_GET["password"] ?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 row mx-auto mt-2">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="card-header">Mes de Acceso</div>
                            <div class="card-body">
                                <p class="card-text"><?= $_GET["mes"] ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="card-header">Formas de Acceso</div>
                            <div class="card-body">
                                <?php
                                if (!isset($_GET["formas"])) {
                                    echo "<p class=\"card-text\">No has seleccionado ninguna forma de acceso</p>";
                                } else {
                                    foreach ($_GET["formas"] as $valor) {
                                        echo "<p class=\"card-text\">$valor</p>";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 row mx-auto mt-2">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="card-header">Ciudad</div>
                            <div class="card-body">
                                <p class="card-text"><?= $_GET["ciudad"] ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="card-header">Navegadores Utilizados</div>
                            <div class="card-body">
                                <?php
                                if (!isset($_GET["navegador"])) {
                                    echo "<p class=\"card-text\">no hay navegadores seleccionados</p>";
                                } else {
                                    foreach ($_GET["navegador"] as $valor) {
                                        echo "<p class=\"card-text\">$valor</p>";
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <form>
                    <div class="form-group row">
                        <label for="correo" class="col-lg-2 col-form-label">Email</label>
                        <div class="col-lg-10">
                            <input type="email" class="form-control" id="correo" placeholder="Email" name="correo" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-lg-2 col-form-label">Password</label>
                        <div class="col-lg-10">
                            <input type="password" class="form-control" id="password" placeholder="contraseña" name="password" required>
                        </div>
                    </div>
                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-lg-2 pt-0">Mes de acceso</legend>
                            <div class="col-lg-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="mes" id="enero" value="enero" checked>
                                    <label class="form-check-label" for="enero">
                                        Enero
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="mes" id="febrero" value="febrero">
                                    <label class="form-check-label" for="febrero">
                                        Febrero
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="mes" id="marzo" value="marzo">
                                    <label class="form-check-label" for="marzo">
                                        Marzo
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-lg-2 pt-0">Formas de acceso</legend>
                            <div class="col-lg-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="formas[]" id="telefono" value="telefono" checked>
                                    <label class="form-check-label" for="telefono">
                                        Telefono
                                    </label>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="formas[]" id="ordenador" value="ordenador">
                                    <label class="form-check-label" for="ordenador">
                                        Ordenador
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-group row">
                        <label for="ciudad" class="col-lg-2 col-form-label">Ciudad</label>
                        <div class="col-lg-10">
                            <select id="ciudad" class="form-control" name="ciudad" required>
                                <option value="" disabled selected>Selecciona una ciudad</option>
                                <option value="Santander">Santander</option>
                                <option value="Laredo">Laredo</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="navegador" class="col-lg-2 col-form-label">Navegadores Utilizados</label>
                        <div class="col-lg-10">
                            <select id="navegador" class="form-control" name="navegador[]" multiple>
                                <option value="" disabled selected>Selecciona navegadores</option>
                                <option value="Google">Google</option>
                                <option value="Edge">Edge</option>
                                <option value="Safari">Safari</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button name="ejercicio2" class="btn btn-primary">Enviar</button>
                        </div>
                    </div>
                </form>

                <?php
            }
            ?>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>