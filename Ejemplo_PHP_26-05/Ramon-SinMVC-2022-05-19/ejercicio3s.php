<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <title>Ejercicio 3 - suma</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><i class="fa fa-home" aria-hidden="true"></i>Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ejercicio1.php">Ejercicio 1</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ejercicio2.php">Ejercicio 2</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ejercicio 3
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="ejercicio3s.php">Suma</a>
                            <a class="dropdown-item" href="ejercicio3p.php">Producto</a>
                            <a class="dropdown-item" href="ejercicio3.php">Ambos</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ejercicio 4
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="ejercicio41.php">Fotos 1</a>
                            <a class="dropdown-item" href="ejercicio42.php">Fotos 2</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="my-4 container-fluid">
            <?php
            if (isset($_GET["ejercicio3s"])) {
                $numeros = $_GET["numeros"];
                $suma = 0;
                $resto = explode(";", $numeros[2]);
                unset($numeros[2]);
                $numeros = array_merge($numeros, $resto);
                foreach ($numeros as $numero) {
                    $suma += $numero;
                }
                ?>
                <div class="col-lg-8 row mx-auto">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="card-header">Suma</div>
                            <div class="card-body">
                                <p class="card-text"><?= $suma ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="card-header">Numeros introducidos</div>
                            <div class="card-body">
                                <?php
                                foreach ($numeros as $valor) {
                                    echo "<p class=\"card-text\">$valor</p>";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <form>
                    <div class="form-group row">
                        <label for="numero1" class="col-lg-2 col-form-label">Numero 1</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control" id="numero1" placeholder="Numero entero" name="numeros[]" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="numero2" class="col-lg-2 col-form-label">Numero 2</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control" id="numero2" placeholder="Numero entero" name="numeros[]" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="numeros" class="col-lg-2 col-form-label">Numeros</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="numeros" placeholder="Numeros separados por ;" name="numeros[]">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <button name="ejercicio3s" class="btn btn-primary">Sumar</button>
                        </div>
                    </div>
                </form>
                <?php
            }
            ?>
            <!-- Optional JavaScript -->
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>