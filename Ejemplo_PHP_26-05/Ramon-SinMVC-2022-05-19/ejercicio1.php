<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <title>Ejercicio 1</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><i class="fa fa-home" aria-hidden="true"></i>Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ejercicio1.php">Ejercicio 1</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ejercicio2.php">Ejercicio 2</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ejercicio 3
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="ejercicio3s.php">Suma</a>
                            <a class="dropdown-item" href="ejercicio3p.php">Producto</a>
                            <a class="dropdown-item" href="ejercicio3.php">Ambos</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ejercicio 4
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="ejercicio41.php">Fotos 1</a>
                            <a class="dropdown-item" href="ejercicio42.php">Fotos 2</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="my-4 container-fluid">
            <?php
            if (isset($_GET["ejercicio1"])) {
                ?>
                <div class="col-lg-8 row mx-auto">
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="card-header">Nombre</div>
                            <div class="card-body">
                                <p class="card-text"><?= $_GET["nombre"] ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card bg-light">
                            <div class="card-header">Apellidos</div>
                            <div class="card-body">
                                <p class="card-text"><?= $_GET["apellidos"] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <form class="col-lg-8 my-5 mx-auto">
                    <div class="form-group row">
                        <label class="col-lg-1 col-form-label pl-0" for="nombre">Nombre:</label>
                        <input type="text" class="col-lg form-control" id="nombre" placeholder="Introduce el nombre" name="nombre">
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-1 col-form-label pl-0" for="apellidos">Apellidos:</label>
                        <input type="text" class="col-lg form-control" placeholder="Introduce apellidos" name="apellidos">
                    </div>
                    <div class="row form-group justify-content-end">
                        <button name="ejercicio1" class="btn btn-primary">Enviar</button>
                    </div>

                </form>

                <?php
            }
            ?>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </body>
</html>