<form class="col-lg-8 my-5 mx-auto">
    <div class="form-group row">
        <label class="col-lg-1 col-form-label pl-0" for="nombre">Nombre:</label>
        <input type="text" class="col-lg form-control" id="nombre" placeholder="Introduce el nombre" name="nombre">
    </div>
    <div class="form-group row">
        <label class="col-lg-1 col-form-label pl-0" for="apellidos">Apellidos:</label>
        <input type="text" class="col-lg form-control" placeholder="Introduce apellidos" name="apellidos">
    </div>
    <div class="row form-group justify-content-end">
        <button name="ejercicio1" class="btn btn-primary">Enviar</button>
    </div>

</form>


