<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio en un archivo php</title>
        <style>
            label{
                background-color: cornflowerblue;
                color: white;
                font-weight: bolder;
            }
        </style>
    </head>
    <body>
        <form method="get">
            <label>NOMBRE :</label><input type="text" name="nombre"><br>
            <label>EDAD :</label><input type="text" name="edad"><br>
            <label>POBLACION :</label><input type="text" name="poblacion"><br>
            <button>Enviar</button>
        </form>
        <?php
        if ($_GET) {
            extract($_GET);

            echo
            '<table border="2">
                <tr>
                    <th>Nombre</th>
                    <td>' . $nombre . '</td>
                </tr>
                <tr>
                    <th>Edad</th>
                    <td>' . $edad . '</td>
                </tr>
                <tr>
                    <th>Poblacion</th>
                    <td>' . $poblacion . '</td>
                </tr>
            </table>';
        }
        ?>
    </body>
</html>