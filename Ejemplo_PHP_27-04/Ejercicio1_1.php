<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio en varios archivos php</title>
    </head>
    <body>
        <?php
        if ($_GET) {
            extract($_GET);
        } else {
            $nombre = 'Vacio';
            $edad = 'Vacio';
            $poblacion = 'Vacio';
        }
        ?>
        <table border="2">
            <tr>
                <th>Nombre</th>
                <td><?= $nombre ?></td>
            </tr>
            <tr>
                <th>Edad</th>
                <td><?= $edad ?></td>
            </tr>
            <tr>
                <th>Poblacion</th>
                <td><?= $poblacion ?></td>
            </tr>
        </table>

    </body>
</html>