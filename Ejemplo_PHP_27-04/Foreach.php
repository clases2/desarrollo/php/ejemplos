<?php
$cajas = ["Parrafo 1", "Parrafo 2", "Parrafo 3"];
$etiquetas = ["codigo", "modelo", "marca"];
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo Foreach</title>
    </head>
    <body>

        <?php
        foreach ($cajas as $valor) {
            echo "<div>$valor</div>";
        }
        ?>

        <form method="get">
            <?php
            foreach ($etiquetas as $c => $valor) {
                // principio bucle para imprimir los controles del formulario
                ?>
                <label for="<?= $etiquetas[$c] ?>"><?= $etiquetas[$c] ?></label>
                <input type="text" id="<?= $etiquetas[$c] ?>" name="<?= $etiquetas[$c] ?>">
                <?php
                // fin del bucle para imprimir los controles del formulario
            }
            ?>
            <button>Enviar</button>
        </form>

        <?php
        ?>
    </body>
</html>
