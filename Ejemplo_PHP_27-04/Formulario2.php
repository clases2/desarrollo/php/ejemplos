<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Formulario 2 todo en un solo archivo php</title>
    </head>
    <body>
        <form method="get">
            <label>Numero 1: </label><input type="text" name="numero1">
            <button>Enviar</button>
        </form>
        <?php
        if ($_GET) {
            //Analizo los datos que vienen del archivo Formulario.php
            $numero1 = $_GET['numero1'];
            //extract($_GET); crea una variable por cada inice de arrays. En este caso crea una variable llamada numero1 igual al valor del input.
            echo $numero1;
        }
        ?>
    </body>
</html>