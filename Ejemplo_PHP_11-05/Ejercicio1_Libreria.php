<?php

function render($radio, $area) {
    include './Ejercicio1_Vista.php';
}

function calcular($radio, &$area) {
    $area = M_PI * pow($radio, 2);
}

function circulo($radio) {
    echo '
        <svg width="100" height="100">
        <circle cx="50%" cy="50%" r="' . ($radio + 10) . '" stroke="green" stroke-width="4" fill="yellow" />
        </svg>';
}
