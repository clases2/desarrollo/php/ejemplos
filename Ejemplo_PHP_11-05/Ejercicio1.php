<?php
require './Ejercicio1_Libreria.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $radio = 0;
        $area = 0;

        if (isset($_GET["calcular"])) {
            $radio = $_GET["radio"];
            calcular($radio, $area);
        }

        render($radio, $area);

        if (isset($_GET["calcular"])) {
            circulo($radio);
        }
        ?>
    </body>
</html>
