<?php
require './cabecera.php';
if (!isset($_GET["ejercicio1"])) {
    ?>
    <div class="container mt-5 text-center">
        <form>
            <p>Nombre: <input type="text" id="nombre" name="nombre" placeholder="Introduce el nombre." /></p>
            <p>Apellidos: <input type="text" id="apellidos" name="apellidos" placeholder="Introduce apellidos." /></p>
            <button class="btn btn-primary" name="ejercicio1">Enviar</button>
        </form> 
    </div>        
    <?php
} else {
    // has pulsado el boton de enviar
    extract($_GET);
    ejercicio1($nombre, $apellidos);
}
require './pie.php';
?>