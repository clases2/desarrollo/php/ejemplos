<?php
require './cabecera.php';
if (!isset($_GET["ejercicio2"])) {
    ?>
    <div class="container mt-5">
        <form>
            <p>Email: <input type="email" id="email" name="email" required="" placeholder="Email." /></p>
            <p>Password: <input type="text" id="contraseña" name="contraseña" required="" placeholder="Contraseña." /></p>
            <div class="row mt-5">
                <div class="col-2"><p>Mes de acceso</p></div>
                <div class="col-10">
                    <input type="radio" id="enero" name="mes" value="Enero"><label for="enero">Enero</label><br>
                    <input type="radio" id="febrero" name="mes" value="Febrero"><label for="febrero">Febrero</label><br>
                    <input type="radio" id="marzo" name="mes" value="Marzo"><label for="marzo">Marzo</label>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-2"><p>Formas de acceso</p></div>
                <div class="col-10">
                    <input type="checkbox" id="telefono" name="acceso[]" value="Telefono"><label for="telefono">Telefono</label><br>
                    <input type="checkbox" id="ordenador" name="acceso[]" value="Ordenador"><label for="ordenador">Ordenador</label>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-2"><label for="ciudad">Ciudad</label></div>
                <div class="col-10">
                    <select id="ciudad" name="ciudad" required="" >
                        <option value="ciudad1">Ciudad1</option>
                        <option value="ciudad2">Ciudad2</option>
                        <option value="ciudad3">Ciudad3</option>
                        <option value="ciudad4">Ciudad4</option>
                    </select>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-2"><label for="navegador[]">Navegadores Utilizados</label></div>
                <div class="col-10">
                    <select id="navegador" name="navegador[]" multiple>
                        <option disabled="">Selecciona los navegadores.</option>
                        <option value="Google Chrome" >Google Chrome</option>
                        <option value="Microsoft Edge">Microsoft Edge</option>
                        <option value="Opera">Opera</option>
                        <option value="Mozilla Firefox">Mozilla Firefox</option>
                    </select>
                </div>
            </div>


            <button class="btn btn-primary mt-5" name="ejercicio2">Enviar</button>
        </form> 
    </div>        
    <?php
} else {
    // has pulsado el boton de enviar
    $acceso = ["No has seleccioando ninguna forma de acceso."];
    $navegador = ["No hay navegadores seleccionados."];
    $mes = "Sin seleccionar.";
    extract($_GET);
    ejercicio2($email, $contraseña, $mes, $acceso, $ciudad, $navegador);
}
require './pie.php';
?>