<?php
require './cabecera.php';
?>
<div class="row">
    <div class="card mb-3 col">
        <img src="Img/1.jpg" class="card-img-top" data-bs-toggle="modal" data-bs-target="#foto1" alt="...">
        <div class="card-body">
            <h5 class="card-title">Foto 1</h5>
            <p class="card-text">Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar.</p>
            <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
    </div>
    
    <div class="modal fade" id="foto1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="Img/1.jpg" alt="" class="img-fluid" />
                </div>
            </div>
        </div>
    </div>
    
    <div class="card mb-3 col">
        <img src="Img/2.jpg" class="card-img-top" data-bs-toggle="modal" data-bs-target="#foto2" alt="...">
        <div class="card-body">
            <h5 class="card-title">Foto 2</h5>
            <p class="card-text">Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar.</p>
            <p class="card-text"><small class="text-muted">Last updated 30 mins ago</small></p>
        </div>
    </div>
    
    <div class="modal fade" id="foto2" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="Img/2.jpg" alt="" class="img-fluid" />
                </div>
            </div>
        </div>
    </div>
    
    <div class="card mb-3 col">
        <img src="Img/3.jpg" class="card-img-top" data-bs-toggle="modal" data-bs-target="#foto3" alt="...">
        <div class="card-body">
            <h5 class="card-title">Foto 3</h5>
            <p class="card-text">Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar.</p>
            <p class="card-text"><small class="text-muted">Last updated 300 mins ago</small></p>
        </div>
    </div>
    
    <div class="modal fade" id="foto3" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="Img/3.jpg" alt="" class="img-fluid" />
                </div>
            </div>
        </div>
    </div>
    
    <div class="card mb-3 col">
        <img src="Img/4.jpg" class="card-img-top" data-bs-toggle="modal" data-bs-target="#foto4" alt="...">
        <div class="card-body">
            <h5 class="card-title">Foto 4</h5>
            <p class="card-text">Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar Texto al azar.</p>
            <p class="card-text"><small class="text-muted">Last updated 3000 years ago</small></p>
        </div>
    </div>
    
    <div class="modal fade" id="foto4" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="Img/4.jpg" alt="" class="img-fluid" />
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require './pie.php';
?>