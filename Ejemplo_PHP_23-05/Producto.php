<?php
require './cabecera.php';
if (!isset($_GET["producto"])) {
    ?>
    <div class="container mt-5 text-center">
        <form>
            <p>Numero 1: <input type="number" id="n1" name="n1" placeholder="Primer numero." /></p>
            <p>Numero 2: <input type="number" id="n2" name="n2" placeholder="Segundo numero." /></p>
            <p>Numeros: <input type="text" id="numeros" name="n3" placeholder="Numeros separados por ;" /></p>
            <button class="btn btn-primary" name="producto">Multiplicar</button>
        </form> 
    </div>        
    <?php
} else {
 
    // has pulsado el boton de enviar
    extract($_GET);
    $numeros = explode(";", $n3);
    producto($n1, $n2, $numeros);
}
require './pie.php';
?>