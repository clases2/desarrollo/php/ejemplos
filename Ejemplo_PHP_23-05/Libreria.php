<?php

function ejercicio1($nombre, $apellidos) {
    echo "<div class='container text-center mt-5'>";
    echo
    "<div class = 'card border-dark mb-3' style = 'max-width: 18rem;'>
        <div class = 'card-header'>Nombre</div>
        <div class = 'card-body text-dark'>
            <p class = 'card-text'>$nombre</p>

        </div>
    </div>";
    echo
    "<div class = 'card border-dark mb-3' style = 'max-width: 18rem;'>
        <div class = 'card-header'>Apellidos</div>
        <div class = 'card-body text-dark'>
            <p class = 'card-text'>$apellidos</p>
        </div>
    </div>";
    echo "</div>";
}

function ejercicio2($email, $contraseña, $mes, $acceso, $ciudad, $navegador) {
    echo "<div class='container text-center mt-5 row'>";

    echo
    "<div class = 'card border-dark mb-3' style = 'max-width: 18rem;'>
        <div class = 'card-header'>Nombre</div>
        <div class = 'card-body text-dark'>
            <p class = 'card-text'>$email</p>

        </div>
    </div>";

    echo
    "<div class = 'card border-dark mb-3' style = 'max-width: 18rem;'>
        <div class = 'card-header'>Mes de acceso</div>
        <div class = 'card-body text-dark'>
            <p class = 'card-text'>$mes</p>
        </div>
    </div>";

    echo
    "<div class = 'card border-dark mb-3' style = 'max-width: 18rem;'>
        <div class = 'card-header'>Ciudad</div>
        <div class = 'card-body text-dark'>
            <p class = 'card-text'>$ciudad</p>
        </div>
    </div>";

    echo
    "<div class = 'card border-dark mb-3' style = 'max-width: 18rem;'>
        <div class = 'card-header'>Password</div>
        <div class = 'card-body text-dark'>
            <p class = 'card-text'>$contraseña</p>
        </div>
    </div>";

    echo
    "<div class = 'card border-dark mb-3' style = 'max-width: 18rem;'>
        <div class = 'card-header'>Formas de Acceso</div>
        <div class = 'card-body text-dark'>";
    foreach ($acceso as $value) {
        echo "<p class = 'card-text'>$value</p>";
    }
    echo"
        </div>
    </div>";

    echo
    "<div class = 'card border-dark mb-3' style = 'max-width: 18rem;'>
        <div class = 'card-header'>Navegadores Utilizados</div>
        <div class = 'card-body text-dark'>";
    foreach ($navegador as $value) {
        echo "<p class = 'card-text'>$value</p>";
    }
    echo"
        </div>
    </div>";

    echo "</div>";
}

function suma($n1, $n2, $n3) {

    $n4 = 0;
    foreach ($n3 as $value) {
        $n4 += $value;
    }
    $resultado = $n1 + $n2 + $n4;
    imprimir($n1,$n2,$n3,$resultado,"Suma");
}

function producto($n1, $n2, $n3) {

    $n4 = 1;
    foreach ($n3 as $value) {
        $n4 *= $value;
    }
    $resultado = $n1 * $n2 * $n4;
    imprimir($n1,$n2,$n3,$resultado,"Proucto");
}

function imprimir($n1,$n2,$n3,$resultado,$operacion){
    
    echo "<div class='container text-center m-5 row'>";
    echo
    "<div class = 'card border-dark mb-3 col-6' style = 'max-width: 18rem;'>
        <div class = 'card-header'>$operacion</div>
        <div class = 'card-body text-dark'>
            <p class = 'card-text'>$resultado</p>

        </div>
    </div>";
    echo
    "<div class = 'card border-dark mb-3 col-6' style = 'max-width: 18rem;'>
        <div class = 'card-header'>Numeros Introducidos</div>
        <div class = 'card-body text-dark'>
        <p class = 'card-text'>$n1</p>
        <p class = 'card-text'>$n2</p>";
    foreach ($n3 as $value) {
        echo "<p class = 'card-text'>$value</p>";
    }
    echo "</div>
    </div>";
    echo "</div>";
}
