<?php
require './cabecera.php';
if (!isset($_GET["producto"]) && !isset($_GET["suma"]) && !isset($_GET["todo"])) {
    ?>
    <div class="container mt-5 text-center">
        <form>
            <p>Numero 1: <input type="number" id="n1" name="n1" placeholder="Primer numero." /></p>
            <p>Numero 2: <input type="number" id="n2" name="n2" placeholder="Segundo numero." /></p>
            <p>Numeros: <input type="text" id="numeros" name="n3" placeholder="Numeros separados por ;" /></p>
            <button class="btn btn-primary" name="suma">Sumar</button> <button class="btn btn-primary" name="producto">Multiplicar</button> <button class="btn btn-primary" name="todo">Todo</button>
        </form> 
    </div>        
    <?php
}

if (isset($_GET["suma"])) {
    // has pulsado el boton de suma.
    extract($_GET);
    $numeros = explode(";", $n3);
    suma($n1, $n2, $numeros);
} else if (isset($_GET["producto"])) {
    // has pulsado el boton de producto.
    extract($_GET);
    $numeros = explode(";", $n3);
    producto($n1, $n2, $numeros);
} else if (isset($_GET["todo"])) {
    // has pulsado el boton de todo.
    extract($_GET);
    $numeros = explode(";", $n3);
    suma($n1, $n2, $numeros);
    producto($n1, $n2, $numeros);
}
require './pie.php';
?>